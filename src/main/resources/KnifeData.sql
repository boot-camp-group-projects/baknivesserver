INSERT INTO categories (id, name)
VALUES (1, 'Combat Knives')
      ,(2, 'Custom Knives')
      ,(3, 'Automatic Knives')
      ,(4, 'Fixed Blade Knives')
      ,(5, 'Hunting Knives')
      ,(6, 'Swords')
      ,(7, 'Pens');

INSERT INTO manufacturers(id, name, address, zipcode, picture)
VALUES(1, 'Benchmade', '300 Beavercreek Road Oregon City, OR', '97045', '../../assets/logo/BenchmadeLogo.png')
      ,(2, 'Microtech','15A National Avenue Fletcher, North Carolina', '28732', '../../assets/logo/MicrotechLogo.png')
      ,(3, 'We Knife Company', '3rd Yuyuan Road, Yangdong Industrial Park, Yangjiang, Guangdong, China', '529500', '../../assets/logo/WeKnifeLogo.png')
      ,(4, 'Spyderco', '820 Spyderco Way Golden CO', '80403', '../../assets/logo/SpydercoLogo.png')
      ,(5, 'Chris Reeve','2949 S Victory View Way Boise, ID', '83709', '../../assets/logo/CRKLogo.png');

INSERT INTO products(name, description, picture, price, sku, category_id, manufacturer_id)
VALUES ('Tan Currahee Combat Fixed'
        , 'Microtech 102-2TA Tan Currahee Combat Fixed 4.5" Single Combo Edge Blade, Kydex Sheath'
        ,'../../assets/productImages/MT1022TA.jpeg'
        ,297.46
        ,'MT1022TA'
        ,1
        ,2),
       ('Signature Series Halo VI'
       ,'Microtech 250-1DLCBLS Signature Series Halo VI AUTO OTF 4.4" Black DLC Tanto Plain Blade, Blue Aluminum Handles'
       ,'../../assets/productImages/MT2501DLCBLS.jpeg'
       ,735.00
       ,'MT2501DLCBLS'
       ,3
       ,2),
       ('Signature Series Troodon'
       ,'Microtech 139-16TI Signature Series Troodon AUTO OTF 3.06" Damascus Drop Point Blade, Black Aluminum Handles, Blue Titanium Hardware'
       ,'../../assets/productImages/MT13916TI.jpeg'
       ,680.00
       ,'MT13916'
       ,3
       ,2),
       ('Cypher MK7'
       ,'Microtech/DC Munroe 242M-1RDBK Cypher MK7 OTF AUTO 4.125" Red Double Edge Blade, Milled Black Aluminum Handles and Hardware - Limited Run of 100'
       ,'../../assets/productImages/MT242M1RDBK.jpeg'
       ,500.00
       ,'MT242M1RDBK'
       ,3
       ,2),
       ('Jagdkommando'
       ,'Microtech 105-10 Jagdkommando Fixed 7" Tri-Edge Dagger, Stonewashed, Black Aluminum Sheath'
       ,'../../assets/productImages/MT10510.jpeg'
       ,999.00
       ,'MT10510'
       ,4
       ,2),
       ('Siphon II Black Pen'
       ,'Microtech Siphon II Black Stainless Steel Pen with Pocket Clip, Black Ink - 401SSBLK'
       ,'../../assets/productImages/MT401SSBLK.jpeg'
       ,159.00
       ,'MT401SSBLK'
       ,7
       ,2),
       ('Vaquita Fixed Blade'
       , 'We Knife Company 807A Vaquita Mini Fixed Blade Knife 3.2,CPM-S35VN Two-Tone, Carbon Fiber Handles, Kydex Sheath'
       , '../../assets/productImages/WEK807A.jpeg'
       , 128.00
       , 'WEK807A'
       , 4
       , 3),

       ('Vindex Fixed Blade'
       , 'We Knife Company 802A Vindex Fixed Blade Knife 4.32" D2 Stonewashed Two-Tone, Green G10 Handles, Kydex Sheath'
       , '../../assets/productImages/WEK802A.jpeg'
       , 138.00
       , 'WEK802A'
       , 4
       , 3),

       ('Vindex Fixed Blade'
       , 'We Knife Company 802B Vindex Fixed Blade Knife 4.32" D2 Black Two-Tone, Black G10 Handles, Kydex Sheath'
       , '../../assets/productImages/WEK802B.jpeg'
       , 138.00
       , 'WEK802B'
       , 4
       , 3),

       ('Vaquita Fixed Blade'
       , 'We Knife Company 807B Vaquita Mini Fixed Blade Knife 3.2" CPM-S35VN Black Two-Tone, Carbon Fiber Handles, Kydex Sheath'
       , '../../assets/productImages/WEK807B.jpeg'
       ,138.00
       , 'WEK807B'
       , 4
       , 3),

       (' 607A Fixed Blade'
       , 'We Knife Company 607A Fixed 4" S35VN Black Stonewashed Blade, Carbon Fiber Handles, Kydex Sheath'
       , '../../assets/productImages/WEK607A.jpeg'
       , 210.00
       , 'WEK607A'
       , 4
       , 3),

       ('607C Fixed Blade'
       , 'We Knife Company 607C Fixed 4" S35VN Satin Blade, Carbon Fiber Handles, Kydex Sheath'
       , '../../assets/productImages/WEK607C.jpeg'
       , 210.00
       , 'WEK607C'
       , 4
       , 3),

       ('Ronin 2'
       , 'Spyderco Ronin 2 Fixed 4.12" CTS-BD1 Wharncliffe Blade, G10 Handles - FB09GP2'
       , '../../assets/productImages/SPFB09GP2.jpeg'
       , 155.00
       , 'SPFB09GP2'
       , 1
       , 4),

       ('Black Aqua Salt'
       , 'Spyderco Aqua Salt Fixed 4.78" H1 Black Serrated Blade, FRN Handles, Polymer Sheath - FB23SBBK'
       , '../../assets/productImages/SPFB23SBBK.jpeg'
       , 190.00
       , 'SPFB23SBBK.jpeg'
       , 4
       , 4),

       ('Bob Lum Darn Dao'
       , 'Spyderco Bob Lum Darn Dao Fixed 10.63" CPM-154 Satin Plain Blade, Black G10 Handles, Leather Sheath, Flash Batch - FB41GP'
       , '../../assets/productImages/SPFB41GP.jpeg'
       , 650.00
       , 'SPFB41GP'
       , 1
       , 4),

       ('Enuff Leaf'
       , 'Spyderco Enuff Leaf Fixed 2.75" VG10 Plain Blade, Black FRN Handles - FB31PBK'
       , '../../assets/productImages/SPFB31PBK.jpeg'
       , 180.00
       , 'SPFB31PBK'
       , 5
       , 4),

#        ('BaliYo Flip Pen',
#         'All of the refined symmetry and weight ratios allow for great spins, flips and twirls',
#         '../../assets/productImages/SPYCN100.jpeg'
#         , 10.00
#         , 'SPYCN100'
#         , 7
#         , 4),
#
#        ('BaliYo Flip Pen'
#        , 'Improved spring-steel clips on both handles allow the BaliYo to be clipped securely in a pocket, but are also easily removable to suit your personal style of flipping.'
#        , '../../assets/productImages/SPYUS100HD.jpeg'
#        , 50.00
#        , 'SPYUS100HD'
#        , 7
#        , 4),
#
#        ('BaliYo Flip Pen'
#        ,'Improved spring-steel clips on both handles allow the BaliYo to be clipped securely in a pocket, but are also easily removable to suit your personal style of flipping.'
#        , '../../assets/productImages/SPYUS116.jpeg'
#        , 50.00
#        , 'SPYUS116'
#        , 7
#        , 4),

       ('Green Beret Combat Knife'
       , 'Chris Reeve Green Beret Combat Knife Fixed 7" S35VN Black Combo Blade, Micarta Handles, Black Nylon Sheath - GB7-1001'
       , '../../assets/productImages/CRKGBBLKS.jpeg'
       , 325.00
       , 'CRKGBBLKS'
       , 1
       , 5),

       ('Pacific Combat Knife'
       , 'Chris Reeve Pacific Combat Knife Fixed 6" S35VN Flat Dark Earth Plain Blade, Micarta Handles, Camo Nylon Sheath - PAC-1004'
       , '../../assets/productImages/CRKPAC1004.jpeg'
       , 325.00
       , 'CRKPAC1004'
       , 1
       , 5),

       ('Nyala Classic Skinner Knife'
       , 'Chris Reeve Nyala Classic Skinner Fixed Blade Knife 3.75" S35VN Stonewashed, Black Canvas Micarta Handles, Leather Sheath - NYA-1002'
       , '../../assets/productImages/CRKNYA1002.jpeg'
       , 250.00
       , 'CRKNYA1002'
       , 4
       , 5),

       ('Chris Reeve Nyala Classic Skinner'
       , 'Chris Reeve Nyala Classic Skinner Fixed 3.75" S35VN Stonewashed Blade, Brown Canvas Micarta Handles, Leather Sheath - NYA-1000'
       , '../../assets/productImages/CRKNYALA.jpeg'
       , 250.00
       , 'CRKNYALA'
       , 4
       , 5),

       ('Large Sebenza 21'
       ,'Chris Reeve Large Sebenza 21 Unique Graphic CGG 3.625" Raindrop Damascus Blade, Milled Titanium Handles with Opal Mosaic Triplet Cabochon Inlay - L21-1058'
       ,'../../assets/productImages/CRKL211058.jpeg'
       ,800.00
       ,'CRKL211058'
       ,2
       ,5),
       ('Gold Class Foray AXIS'
       ,'Benchmade 698-181 Gold Class Foray AXIS Folding Knife 3.22" Loki Damasteel Blade, Marbled Carbon Fiber Handles with Mother of Pearl Inlays'
       ,'../../assets/productImages/BM698181.jpeg'
       ,800.00
       ,'BM698181'
       ,2
       ,1);

INSERT INTO customers (id, name, address, zipcode, email, phone, password)
VALUES (1,'Crocodile Dundee', 'ausie road', '84040', 'ausie@gmail.com', '911', '1234')
     ,( 2,'Rambo', 'travel ', '84040', 'rambo@gmail.com', '411', '1234');


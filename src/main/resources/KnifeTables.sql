CREATE TABLE manufacturers (
                             id INT NOT NULL AUTO_INCREMENT,
                             name VARCHAR(255),
                             address VARCHAR(255),
                             zipcode VARCHAR(10),
                             picture VARCHAR(255),
                             PRIMARY KEY (id)
);

CREATE TABLE customers (
                         id INT NOT NULL AUTO_INCREMENT,
                         name VARCHAR(255),
                         address VARCHAR(255),
                         zipcode VARCHAR(10),
                         email VARCHAR(255),
                         phone VARCHAR(255),
                         password VARCHAR(255),
                         PRIMARY KEY (id)
);

CREATE TABLE categories (
                          id INT  NOT NULL AUTO_INCREMENT,
                          name VARCHAR(255),
                          description VARCHAR(255),
                          PRIMARY KEY (id)
);

CREATE TABLE products(
                       id INT NOT NULL AUTO_INCREMENT,
                       name VARCHAR(255),
                       description TEXT,
                       picture VARCHAR(255),
                       price DOUBLE,
                       sku VARCHAR(255),
                       category_id INT,
                       manufacturer_id INT,
                       PRIMARY KEY (id),
                       CONSTRAINT FKgro094vh0dp0tly1225wk8u37 FOREIGN KEY (category_id)
                         REFERENCES categories(id),
                       CONSTRAINT FKt7krekmftw52jenbn9ddasw28 FOREIGN KEY (manufacturer_id)
                         REFERENCES manufacturers(id)
);

CREATE TABLE orders (
                      id INT NOT NULL AUTO_INCREMENT,
                      quantity INT,
                      date DATE,
                      customer_id INT,
                      product_id INT,
                      PRIMARY KEY (id),
                      CONSTRAINT FKpxtb8awmi0dk6smoh2vp1litg FOREIGN KEY (customer_id)
                        REFERENCES customers(id),
                      CONSTRAINT FKkp5k52qtiygd8jkag4hayd0qg FOREIGN KEY (product_id)
                        REFERENCES products(id)

);


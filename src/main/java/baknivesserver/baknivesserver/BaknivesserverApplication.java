package baknivesserver.baknivesserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaknivesserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaknivesserverApplication.class, args);
    }

}

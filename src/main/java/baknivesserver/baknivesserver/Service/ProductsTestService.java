package baknivesserver.baknivesserver.Service;

import baknivesserver.baknivesserver.model.ProductModel;
import baknivesserver.baknivesserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductsTestService {

    @Autowired
    private ProductRepository productRepo;

    public Iterable<ProductModel> getAllProducts(){
        return productRepo.findAll();
    }
}

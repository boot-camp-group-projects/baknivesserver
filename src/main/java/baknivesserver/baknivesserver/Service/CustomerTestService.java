package baknivesserver.baknivesserver.Service;

import baknivesserver.baknivesserver.model.CustomerModel;
import baknivesserver.baknivesserver.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerTestService {
    @Autowired
    private CustomerRepository customerRepo;

    public Iterable<CustomerModel> getAllCustomers(){return customerRepo.findAll();}
}

package baknivesserver.baknivesserver.controller;

import baknivesserver.baknivesserver.model.ProductModel;
import baknivesserver.baknivesserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    private ProductRepository productRepo;

    public ProductController(ProductRepository productRepo){this.productRepo = productRepo;}

    @GetMapping("/products")
    public List<ProductModel> getAllProducts() {return (List<ProductModel>) this.productRepo.findAll();}

    @GetMapping("/products/{id}")
    public ProductModel getProductById(@PathVariable Long id) {return this.productRepo.findAllById(id);}

    @GetMapping("/products/categories/{id}")
    public List<ProductModel> getProductByCategoryId(@PathVariable Long id) {return this.productRepo.findProductModelByCategoriesModel_Id(id);}

    @GetMapping("/products/manufacturers/{id}")
    public List<ProductModel> getProductByManufacturerId(@PathVariable Long id) {return this.productRepo.findProductModelByManufacturersModel_Id(id);}

    @GetMapping("/products/random5")
    public List<ProductModel> get5RandomProducts() {
        return this.productRepo.find5RandomProducts();
    }

}




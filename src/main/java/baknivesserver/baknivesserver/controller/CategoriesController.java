package baknivesserver.baknivesserver.controller;

import baknivesserver.baknivesserver.model.CategoriesModel;
import baknivesserver.baknivesserver.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class CategoriesController {
    @Autowired
    private CategoriesRepository categoriesRepo;
    @Autowired
    public CategoriesController(CategoriesRepository categoriesRepo) {
        this.categoriesRepo = categoriesRepo;
    }

    @GetMapping("/categories")
    public List<CategoriesModel> getAllCategories() {return (List<CategoriesModel>) this.categoriesRepo.findAll();}

    @GetMapping("/categories/{id}")
    public CategoriesModel getCategoryById(@PathVariable Long id) {return this.categoriesRepo.findAllById(id);}
}

package baknivesserver.baknivesserver.controller;

import baknivesserver.baknivesserver.model.ManufacturersModel;
import baknivesserver.baknivesserver.repository.ManufacturersRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ManufacturersController {
    private ManufacturersRepository manufacturersRepo;

    public ManufacturersController(ManufacturersRepository manufacturersRepo) {
        this.manufacturersRepo = manufacturersRepo;
    }

    @GetMapping(value = "/manufacturers")
    public List<ManufacturersModel> getAllManufacturers() {
        return (List<ManufacturersModel>) manufacturersRepo.findAll();
    }


    @GetMapping("/manufacturers/{id}")
    public ManufacturersModel getProductsByManufacturerById(@PathVariable Long id) {return this.manufacturersRepo.findAllById(id);}

}

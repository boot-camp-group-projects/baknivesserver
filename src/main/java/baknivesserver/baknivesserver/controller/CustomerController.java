package baknivesserver.baknivesserver.controller;

import baknivesserver.baknivesserver.Service.CustomerTestService;
import baknivesserver.baknivesserver.model.CustomerModel;
import baknivesserver.baknivesserver.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {
    private CustomerRepository customerRepo;
    public CustomerController(CustomerRepository customerRepo){this.customerRepo = customerRepo;}

    //test crap
    @Autowired
    private CustomerTestService customerService;

    @GetMapping(value = "/customers")
    public List<CustomerModel> getAllCustomers() {
        return (List<CustomerModel>) customerService.getAllCustomers();
    }

    @GetMapping("/customers/{id}")
    public CustomerModel getCategoryById(@PathVariable Long id) {return this.customerRepo.findAllById(id);}

    @PostMapping("/customers/create")
    public CustomerModel createStudent(@RequestBody CustomerModel creatme) {

        customerRepo.save(creatme);
        return creatme;
    }
}

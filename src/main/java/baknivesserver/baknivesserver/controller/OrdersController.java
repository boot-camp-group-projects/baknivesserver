package baknivesserver.baknivesserver.controller;

import baknivesserver.baknivesserver.model.OrdersModel;
import baknivesserver.baknivesserver.repository.OrdersRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class OrdersController {
    private OrdersRepository orderRepo;

    public OrdersController(OrdersRepository orderRepo) {
        this.orderRepo = orderRepo;
    }

    @GetMapping("/orders")
    public List<OrdersModel> getAllOrders() {return (List<OrdersModel>) this.orderRepo.findAll();}

    @GetMapping("/orders/{id}")
    public OrdersModel getOrdersById(@PathVariable Long id) {return this.orderRepo.findAllById(id);}
}

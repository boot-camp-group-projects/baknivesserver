package baknivesserver.baknivesserver.repository;

import baknivesserver.baknivesserver.model.CategoriesModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends CrudRepository<CategoriesModel, Long> {

    public CategoriesModel findAllById(Long id);
}

package baknivesserver.baknivesserver.repository;

import baknivesserver.baknivesserver.model.OrdersModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends CrudRepository<OrdersModel, Long> {

    public OrdersModel findAllById(Long id);
}

package baknivesserver.baknivesserver.repository;

import baknivesserver.baknivesserver.model.ManufacturersModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturersRepository extends CrudRepository<ManufacturersModel, Long> {

    public ManufacturersModel findAllById(Long id);
}

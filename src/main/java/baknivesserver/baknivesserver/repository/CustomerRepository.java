package baknivesserver.baknivesserver.repository;

import baknivesserver.baknivesserver.model.CustomerModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<CustomerModel,Long> {

    public CustomerModel findAllById(Long id);
}

package baknivesserver.baknivesserver.repository;

import baknivesserver.baknivesserver.model.ProductModel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductModel, Long> {

    public ProductModel findAllById(Long id);
    public List<ProductModel> findProductModelByCategoriesModel_Id(Long id);
    public List<ProductModel> findProductModelByManufacturersModel_Id(Long id);

    @Query(value = "SELECT p.* " +
            "FROM products p " +
            "ORDER BY RAND() " +
            "LIMIT 5; ", nativeQuery = true)
    public List<ProductModel> find5RandomProducts();


}


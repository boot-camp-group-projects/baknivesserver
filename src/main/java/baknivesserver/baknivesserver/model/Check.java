package baknivesserver.baknivesserver.model;

import java.security.InvalidParameterException;

public class Check {
    private double subtotal;
    private int partySize;

    public Check() {}

    public Check(int partySize, double subtotal){
        this.setPartySize(partySize);
        this.setSubtotal(subtotal);
    }

    public double getSubtotal(){
        return subtotal;
    }

    public void setSubtotal(double subtotal){
        this.subtotal = subtotal;
    }

    public int getPartySize() {
        return partySize;
    }

    public void setPartySize(int partySize) {
        if(partySize<=0){
            throw new InvalidParameterException(partySize + " is not a valid party size");
        }
        this.partySize = partySize;
    }

    public double calculateTotal(){
        if(this.partySize >=6){
            return this.subtotal + calculateEighteenPercentTip();
        }
        else {
            return this.subtotal;
        }

    }

    public double calculateFifteenPercentTip() {
            return calculateTip(15);
    }
    public double calculateEighteenPercentTip() {
        return calculateTip(18);
    }
    public double calculateTwentyPercentTip() {
        return calculateTip(20);
    }
    public double calculateTip(double percent) {
        return Math.round(this.subtotal * percent) / 100.0;
    }
}

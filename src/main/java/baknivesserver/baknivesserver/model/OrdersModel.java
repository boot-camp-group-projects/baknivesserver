package baknivesserver.baknivesserver.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
@Table(name = "orders")
public class OrdersModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer quantity;

    @Column
    private Date date;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private CustomerModel customerModel;

    @ManyToOne
    @JoinColumn(name = "productId", referencedColumnName = "id")
    private ProductModel productModel;

}

package baknivesserver.baknivesserver.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "products")
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "picture")
    private String picture;
    @Column(name = "price")
    private Double price;
    @Column(name = "sku")
    private String sku;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private CategoriesModel categoriesModel;

    @ManyToOne
    @JoinColumn(name = "manufacturerId")
    private ManufacturersModel manufacturersModel;
}


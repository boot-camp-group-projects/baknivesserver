package baknivesserver.baknivesserver;

import baknivesserver.baknivesserver.Service.CustomerTestService;
import baknivesserver.baknivesserver.controller.CustomerController;
import baknivesserver.baknivesserver.model.CustomerModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CustomerController.class)
@ContextConfiguration(classes = {BAKnivesTestsConfiguration.class})
public class CustomerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @InjectMocks
    private CustomerController customerController;
    @MockBean
    private CustomerTestService customerTestService;
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }
    @Test
    public void getAllCustomers() throws  Exception{
        CustomerModel customer1 = new CustomerModel();
        customer1.setName("John");
        customer1.setEmail("John@gmail.com");
        customer1.setPassword("1234");
        customer1.setAddress("road");
        customer1.setPhone("1-234-5678");

        List<CustomerModel> customerModelList = new ArrayList<>();
        customerModelList.add(customer1);

        Mockito.when(customerTestService.getAllCustomers()).thenReturn(customerModelList);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/customers").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String input = this.mapToJson(customerModelList);
        String output = result.getResponse().getContentAsString();
        assertThat(output).isEqualTo(input);
    }
    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    };
}

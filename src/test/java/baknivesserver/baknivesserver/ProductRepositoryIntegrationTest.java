package baknivesserver.baknivesserver;

import baknivesserver.baknivesserver.controller.ProductController;
import baknivesserver.baknivesserver.model.ProductModel;
import baknivesserver.baknivesserver.repository.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductRepository productRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void findProductById_thenReturnProduct() {
        ProductModel p = new ProductModel();
        p.setId(1L);
        p.setName("Conan Sword");
        p.setDescription("The Destroyer");
        p.setPrice(5999.99);

        Mockito.when(productRepository.findAllById(1L)).thenReturn(p);

        ProductModel product = productController.getProductById(1L);

        Mockito.verify(productRepository).findAllById(1L);

        assertEquals(1L, product.getId().longValue());
        assertEquals("Conan Sword", product.getName());
        assertEquals("The Destroyer", product.getDescription());
        assertEquals(5999.99, product.getPrice(), 0);
    }

    @Test
    public void testFindAllProducts() throws Exception{
        ProductModel p1 = new ProductModel();
        p1.setId(1L);
        p1.setName("Conan Sword");
        p1.setDescription("The Destroyer");
        p1.setPrice(5999.99);

        ProductModel p2 = new ProductModel();
        p2.setId(1L);
        p2.setName("Conan Sword");
        p2.setDescription("The Destroyer");
        p2.setPrice(5999.99);

        List<ProductModel> knifeList = new ArrayList<>();
        knifeList.add(p1);
        knifeList.add(p2);

        Mockito.when(productRepository.findAll()).thenReturn(knifeList);

        List<ProductModel> resultList = productController.getAllProducts();

        Mockito.verify(productRepository).findAll();

        String  URI = "/products";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(resultList);
        String outputJson = result.getResponse().getContentAsString();
        assertEquals(outputJson, expectedJson);

    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}

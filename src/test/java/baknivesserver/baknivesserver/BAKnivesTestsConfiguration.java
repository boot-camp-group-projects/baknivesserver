package baknivesserver.baknivesserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.context.annotation.Primary;

@SpringBootConfiguration
@AutoConfigureTestDatabase
public class BAKnivesTestsConfiguration {
    @Primary
    public static void main(String[] args){
        SpringApplication.run(BaknivesserverApplication.class,args);
    }
}

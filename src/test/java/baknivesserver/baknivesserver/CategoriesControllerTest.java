package baknivesserver.baknivesserver;


import baknivesserver.baknivesserver.Service.CategoriesTestService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CategoriesController.class, secure = false)
@ContextConfiguration(classes = {BAKnivesTestsConfiguration.class})
public class CategoriesControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private  CategoriesController categoriesController;

    @MockBean
    private CategoriesTestService categoriesTestService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(categoriesController).build();
    }

//    @Test
//    public void createCategories() throws Exception {
//        Categories.categories = new Categories();
//        categories.setName
//    }
}

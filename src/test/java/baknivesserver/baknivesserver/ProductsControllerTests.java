package baknivesserver.baknivesserver;


import baknivesserver.baknivesserver.controller.ProductController;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ProductController.class)

public class ProductsControllerTests {
}
